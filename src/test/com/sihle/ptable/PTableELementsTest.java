package test.com.sihle.ptable;
import main.com.sihle.ptable.*;

/**
 * I wrote this using a cloud IDE called codenvy, and have
 * not figured out how to add jUnit as one of the stacks/services
 */

public class PTableELementsTest {
    
    public static String givenElementAndSymbolTestRule(String element, String symbol){
        return new PTableElements().showElementPass(element,symbol);
    }
    
    public static void main(String[] args){
        System.out.println(givenElementAndSymbolTestRule(null, "Ty"));
        System.out.println(givenElementAndSymbolTestRule("Spenglerium", "Ee"));
        System.out.println(givenElementAndSymbolTestRule("Zeddemorium", "Zr"));
        System.out.println(givenElementAndSymbolTestRule("Venkmine", "Kn"));
        System.out.println(givenElementAndSymbolTestRule("Stantzon", "Zt"));
        System.out.println(givenElementAndSymbolTestRule("Melintzum", "Nn"));
        System.out.println(givenElementAndSymbolTestRule("Tullium", "Ty"));
        System.out.println(givenElementAndSymbolTestRule("Tullium", null));
    }
    
}

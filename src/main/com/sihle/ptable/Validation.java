package main.com.sihle.ptable;

public class Validation {
    
    /**
     * Element name must have greater than 2 chars
     */
    boolean validElement(String element){
        return element != null && !element.isEmpty() && element.length() > 2;
    }
    
    /**
     * Symbol name must have 2 chars
     */
    boolean validSymbol(String symbol){
        return symbol != null && !symbol.isEmpty() && symbol.length() == 2;
    }
    
    public boolean valid(String element, String symbol){
        return validElement(element) && validSymbol(symbol);
    }
}

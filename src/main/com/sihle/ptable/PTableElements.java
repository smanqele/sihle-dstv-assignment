package main.com.sihle.ptable;

public class PTableElements {
    
    public boolean symbolPass(String element, String symbol){
        if (new Validation().valid(element, symbol)){
            return new PTableRule().applyRule(element.toLowerCase(), symbol.toLowerCase());
        }
        return false;        
    }
    
    public String showElementPass(String element, String symbol){
        if (new Validation().valid(element, symbol)){
            return element + ", " + symbol + " -> " + symbolPass(element, symbol);
        }

        return "Element or Symbol invalid: " + element + ", " + symbol ; 
    }
    
}

package main.com.sihle.ptable;

public class PTableRule {
    
    /**
     * If the symbolChar is found in the element name then return
     * the name minus the symbolChar, otherwise return an empty string.
     */
    RuleResult symbolRule(String element, char symbolChar){
        for (char el : element.toCharArray()){
            if (el == symbolChar){
                String remainder = element.substring(element.indexOf(el) + 1, element.length());
                return new RuleResult(true, remainder);
            }
        }
        return new RuleResult(false, "");
    }
    
    boolean symbolPass(String element, char symbolChar){
        return symbolRule(element, symbolChar).charFound();
    }
    
    public boolean applyRule(String element, String symbol){
        char[] symbolChars = symbol.toCharArray();
        RuleResult firstPass = symbolRule(element, symbolChars[0]);
        return firstPass.charFound() ? symbolPass(firstPass.remainder(), symbolChars[1]) : false;
    }
    
    public static class RuleResult{
        final boolean charFound;
        final String remainder;
        public RuleResult(boolean found, String remainder){
            this.charFound = found;
            this.remainder = remainder;
        }
        
        public boolean charFound(){
            return this.charFound;
        }
        
        public String remainder(){
            return this.remainder;
        }
    }

}
